﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace XOG
{
    internal class Program
    {
        static string[,] map = new string[3, 3];

        static void Main(string[] args)
        {
            InfoGameShow();

            GetMap();

            string UserTypeStep = "X";
            string RobotTypeStep = "O";


            bool empty = true;
            while (empty)
            {

                ShowMap();


                //ход игрока
                empty = GetEmpty();
                while (empty)
                {
                    Console.Write("\nВведите номер ячейки: ");
                    string userStep = Console.ReadLine();
                    bool stepUs = UserStep(userStep, UserTypeStep);

                    if (stepUs)
                    {
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Неликвидни данні");

                    }

                }

                Console.Clear();
                InfoGameShow();
                ShowMap();

                if (VictoryConditions(UserTypeStep))
                {
                    Console.WriteLine("Победа " + UserTypeStep);
                    break;
                }

                //ход робота
                empty = GetEmpty();
                while (empty)
                {

                    bool stepusRb = RobotStep(RobotTypeStep);

                    if (stepusRb)
                    {
                        break;
                    }

                }

                Console.Clear();
                InfoGameShow();
                ShowMap();

                if (VictoryConditions(RobotTypeStep))
                {

                    Console.WriteLine("Победа " + RobotTypeStep);
                    break;
                }

                Console.Clear();
            }

            if (!empty)
            {
                Console.WriteLine("Походу ничья :(");
                Console.WriteLine("Нічого спробуй ще раз");
            }

            Console.ReadKey();
        }

        static public bool RobotStep(string typeStep)
        {
            Random rand = new Random();

            int namCom = rand.Next(1, 10);
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] == "[" + namCom.ToString() + "]")
                    {
                        map[i, j] = "[" + typeStep + "]";
                        return true;
                    }
                }
            }
            return false;
        }

        static public bool VictoryConditions(string typeStep)
        {

            int len = map.GetLength(0);
            int hori = 0;
            int vert = 0;
            int diogon1 = 0;
            int diogon2 = 0;
            
            for (int i = 0; i < map.GetLength(0); i++)
            {
                hori = 0;
                vert = 0;
                diogon1 = 0;
                diogon2 = 0;

                for (int j = 0; j < map.GetLength(1); j++)
                {

                    if (map[i, j] == "[" + typeStep + "]")
                    {
                        hori++;
                    }

                    if (map[j, i] == "[" + typeStep + "]")
                    {
                        vert++;
                    }

                    if (map[(len - 1) - i, j] == "[" + typeStep + "]")
                    {
                        diogon2++;
                    }

                    if (map[j, j] == "[" + typeStep + "]")
                    {
                        diogon1++;
                    }

                }

                

                if (hori == len || vert == len || diogon1 == len || diogon2 == len)
                {
                    return true;
                }

            }


            return false;
        }

        static public bool UserStep(string step, string typeStep)
        {
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] == "[" + step + "]")
                    {
                        map[i, j] = "[" + typeStep + "]";
                        return true;
                    }
                }
            }
            return false;
        }

        static public void GetMap()
        {

            int namPoz = 1;
            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    map[i, j] = "[" + namPoz + "]";
                    namPoz++;
                }
            }

        }

        static public void ShowMap()
        {

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    Console.Write(map[i, j]);
                }
                Console.WriteLine();
            }
        }

        static public bool GetEmpty()
        {

            for (int i = 0; i < map.GetLength(0); i++)
            {
                for (int j = 0; j < map.GetLength(1); j++)
                {
                    if (map[i, j] != "[X]" && map[i, j] != "[O]")
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        static public void InfoGameShow()
        {
            Console.WriteLine(new String('-', 70));
            Console.WriteLine("Добро пожаловать в игру в хрестики нолики.");
            Console.WriteLine("Гра крива але яка є");
            Console.WriteLine(new String('-', 70));

        }

    }
}